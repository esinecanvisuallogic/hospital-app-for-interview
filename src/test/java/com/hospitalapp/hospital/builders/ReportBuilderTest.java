package com.hospitalapp.hospital.builders;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan on 02/04/2017.
 */
public class ReportBuilderTest {

    @Test
    public void testBuildReport(){
        assertEquals("F:1 H:2 D:3 T:1 X:0"
                , ReportBuilder.build(PatientListBuilder.build("F,H,D,D,D,H,T")));
    }
}
