package com.hospitalapp.hospital.builders;

import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.domain.HealthCondition;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan on 02/04/2017.
 */
public class PatientListBuilderTest {

    @Test
    public void testProperlyFormedInput(){
        List<Patient> patients = PatientListBuilder.build("F,H,D,D,D,H,T");
        assertEquals(patients.size(), 7);
        assertEquals(patients.stream().filter(
                patient -> patient.getCurrentHealthCondition() == HealthCondition.FEVER).count(), 1);
        assertEquals(patients.stream().filter(
                patient -> patient.getCurrentHealthCondition() == HealthCondition.DIABETES).count(), 3);
        assertEquals(patients.stream().filter(
                patient -> patient.getCurrentHealthCondition() == HealthCondition.HEALTHY).count(), 2);
        assertEquals(patients.stream().filter(
                patient -> patient.getCurrentHealthCondition() == HealthCondition.TUBERCULOSIS).count(), 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testmalformedInput(){
        List<Patient> patients = PatientListBuilder.build("FHDDDHT");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testProperlyFormedInput_withUnsupportedOperations(){
        List<Patient> patients = PatientListBuilder.build("F,H,D,D,D,H,J");
    }
}
