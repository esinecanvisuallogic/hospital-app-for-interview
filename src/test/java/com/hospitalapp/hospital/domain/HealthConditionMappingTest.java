package com.hospitalapp.hospital.domain;

import org.junit.Test;

import static com.hospitalapp.hospital.domain.HealthCondition.DEAD;
import static com.hospitalapp.hospital.domain.HealthCondition.getHealthConditionByValue;
import static org.junit.Assert.assertEquals;

/**
 * Created by eren.sinecan on 02/04/2017.
 */
public class HealthConditionMappingTest {

    @Test
    public void testGetByValue(){
        assertEquals(DEAD, getHealthConditionByValue("X"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetByValue_unsupportedType(){
        getHealthConditionByValue("O");
    }

}
