package com.hospitalapp.hospital.builders;

import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.domain.HealthCondition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by eren.sinecan on 02/04/2017.
 */

/**
 * A statically accessed class to parse user input
 */
public class PatientListBuilder {

    /**
     * This static method takes the input String which is a collection
     * of comma separated values, and returns a list that keeps the
     * Patient objects
     * @param subjects
     * @return
     */
    public static List<Patient> build(String subjects){
        final List<Patient> patients = new ArrayList<>();
        Arrays.stream(subjects.split(",")).forEach(s -> {
            Patient patient = new Patient(HealthCondition.getHealthConditionByValue(s));
            patients.add(patient);
        });
        return patients;
    }
}
