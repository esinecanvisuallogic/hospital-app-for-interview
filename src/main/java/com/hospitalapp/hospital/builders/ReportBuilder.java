package com.hospitalapp.hospital.builders;

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;

import java.util.Arrays;
import java.util.List;
/**
 * Created by eren.sinecan on 02/04/2017.
 */
/**
 * A statically accessed class to generate a report
 */
public class ReportBuilder {

    /**
     * This method takes the list of patients, counts them grouped by their current
     * status, and returns the output as a space character separated collection of
     * key-value pairs.
     * @param patients
     * @return
     */
    public static String build(List<Patient> patients){
        StringBuilder reportBuilder = new StringBuilder();
        Arrays.stream(HealthCondition.values()).forEach(healthCondition -> {
            reportBuilder.append(healthCondition.getValue());
            reportBuilder.append(":");
            Long count = patients.stream().filter(patient -> patient.getCurrentHealthCondition() == healthCondition)
                    .count();
            reportBuilder.append(count.intValue());
            reportBuilder.append(" ");
        });
        return reportBuilder.toString().trim();
    }
}
