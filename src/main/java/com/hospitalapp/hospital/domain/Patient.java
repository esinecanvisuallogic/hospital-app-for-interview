package com.hospitalapp.hospital.domain;

import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;

/**
 * This is the patient class where our patient's attributes are held.
 */
@Setter
@Getter
public class Patient {

    public Patient(HealthCondition currentHealthCondition){
        this.currentHealthCondition = currentHealthCondition;
        this.initialHealthCondition = currentHealthCondition;
        this.treatments = new ArrayList<>();
    }

    /**
     * Treatments applied to patient instance
     */
    private List<Treatment> treatments;

    /**
     * Patient's current health condition.
     */
    private HealthCondition currentHealthCondition;

    /**
     * Patient's initial health condition.
     */
    private HealthCondition initialHealthCondition;

    /**
     * This method is just a shorthand for
     * patient.getTreatments().add(treatment)
     * @param treatment
     */
    public void addTreatment(Treatment treatment){
        treatments.add(treatment);
    }
}
