package com.hospitalapp.hospital.domain;

import java.util.Arrays;

/**
 * The enum where all the possible health conditions that someone
 * checked into our system can have are defined. Whenever a new type
 * of illness needs to be added, it should be added here with a unique
 * String value.
 */
public enum HealthCondition {
    FEVER("F"), HEALTHY("H"), DIABETES("D"), TUBERCULOSIS("T"), DEAD("X");

    /**
     * This field is the one letter String value that is used in
     * quarantine initialization and report generation. Keep it unique
     * when extending. No programmatic restriction on length of this field.
     */
    private String value;

    //Let's warn the developer in case of duplicates
    static {
        if(Arrays.stream(HealthCondition.values())
                .map(e -> e.value)
                .distinct()
                .count() < HealthCondition.values().length){
            System.out.println("Warning: There are duplicates in HealthCondition" +
                    " definitions. Program may behave unexpectedly.");
        }
    }

    /**
     * private constructor never called explicitly. Sets the string value
     * of illness.
     * @param value
     */
    private HealthCondition(String value){
        this.value = value;
    }

    /**
     * Looks up HealthCondition from it's String value.
     * Could have been done using a Map<String, HealthCondition>
     * and a static init method, but I wanted to use Java8.
     * @param value value of the enum to be looked up.
     * @return HealthCondition Enum.
     */
    public static HealthCondition getHealthConditionByValue(String value){
        return Arrays.stream(HealthCondition.values())
                .filter(e -> e.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported type %s.", value)));
    }

    /**
     * @return String value of HealthCondition
     */
    public String getValue(){
        return value;
    }
}
