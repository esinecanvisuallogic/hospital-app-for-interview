package com.hospitalapp.hospital.domain;

/**
 * Created by eren.sinecan on 02/04/2017.
 */

import com.hospitalapp.hospital.treatment.TreatmentContract;
import com.google.common.base.CaseFormat;

/**
 * This is the enum that holds our implemented treatment types.
 * A treatment needs to be registered here in order to be exposed
 * to our main class.
 */
public enum Treatment {
    ASPIRIN, ANTIBIOTIC, INSULIN, PARACETAMOL, WAIT_40_DAYS;

    /**
     * This method takes the name of the instance it's in. Then it
     * converts this name from uppercase and underscore to camel case
     * that starts with an uppercase. This gives the class name in
     * implementation package. Since all valid treatment implementations
     * implement TreatmentContract interface, they all have a treat method.
     * then this treat method gets invoked.
     * @param patient
     * @return
     */
    public HealthCondition treat(Patient patient){

        Class treatmentClass;

        try {
            String treatmentClassName = CaseFormat.UPPER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL, this.name());
            treatmentClass = Class.forName("com.hospitalapp.hospital.treatment.impl." + treatmentClassName);
            Object treatmentObj = treatmentClass.newInstance();

            if(treatmentObj instanceof TreatmentContract){
                TreatmentContract treatmentContract = (TreatmentContract) treatmentObj;
                return treatmentContract.treat(patient);
            }else{
                throw new UnsupportedOperationException(treatmentClassName + ".java should implement" +
                        "TreatmentContract");
            }
        } catch (Exception e) {
            String errorMessage = String.format("Error: No working implementation found for treatment: ", this.name());
            e.printStackTrace();
            System.out.println(errorMessage);
            throw new UnsupportedOperationException(errorMessage);
        }
    }
}
