package com.hospitalapp.hospital.treatment;

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;

/**
 * Created by eren.sinecan on 02/04/2017.
 */
/**
 * This is the treatment interface every new treatment method
 * should be coded against. Also, in order to expose a class
 * that implements this contract, its name should be the camel
 * case version of its Treatment enum's name.
 */
public interface TreatmentContract {
    HealthCondition treat(Patient patient);
}
