package com.hospitalapp.hospital.treatment.impl;

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.treatment.TreatmentContract;

import static com.hospitalapp.hospital.domain.HealthCondition.FEVER;
import static com.hospitalapp.hospital.domain.HealthCondition.HEALTHY;
import static com.hospitalapp.hospital.domain.HealthCondition.TUBERCULOSIS;
import static com.hospitalapp.hospital.domain.Treatment.ANTIBIOTIC;
import static com.hospitalapp.hospital.domain.Treatment.INSULIN;

/**
 * Created by eren.sinecan on 02/04/2017.
 */

/**
 * Implementation of Antibiotic treatment method.
 * If a person is initially healthy, antibiotic will give them
 * fever if taken with insulin.
 * If a person has tuberculosis, antibiotic will cure them and will
 * not cause fever if taken with insulin in this case as deduced by
 * test specs.
 * Otherwise antibiotic will not change the health status
 */
public class Antibiotic implements TreatmentContract {

    @Override
    public HealthCondition treat(Patient patient) {
        HealthCondition healthCondition = patient.getCurrentHealthCondition();
        if(patient.getTreatments().contains(INSULIN)
                && patient.getInitialHealthCondition() == HEALTHY){
            healthCondition = FEVER;
        }else{
            if(patient.getCurrentHealthCondition() == TUBERCULOSIS)
            {
                healthCondition = HEALTHY;
            }
        }
        patient.addTreatment(ANTIBIOTIC);
        return healthCondition;
    }
}
