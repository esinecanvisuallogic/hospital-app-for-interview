package com.hospitalapp.hospital.treatment.impl;

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.treatment.TreatmentContract;

import static com.hospitalapp.hospital.domain.HealthCondition.DEAD;
import static com.hospitalapp.hospital.domain.HealthCondition.FEVER;
import static com.hospitalapp.hospital.domain.HealthCondition.HEALTHY;
import static com.hospitalapp.hospital.domain.Treatment.ASPIRIN;
import static com.hospitalapp.hospital.domain.Treatment.PARACETAMOL;

/**
 * Created by eren.sinecan on 02/04/2017.
 */
public class Paracetamol implements TreatmentContract {
    @Override
    public HealthCondition treat(Patient patient) {
        HealthCondition healthCondition = patient.getCurrentHealthCondition();
        if(patient.getTreatments().contains(ASPIRIN)){
            healthCondition = DEAD;
        }else{
            if(patient.getCurrentHealthCondition() == FEVER)
            {
                healthCondition = HEALTHY;
            }
        }
        patient.addTreatment(PARACETAMOL);
        return healthCondition;
    }
}
