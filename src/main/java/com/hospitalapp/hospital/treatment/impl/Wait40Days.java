package com.hospitalapp.hospital.treatment.impl;

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.treatment.TreatmentContract;
import com.hospitalapp.hospital.domain.Treatment;

/**
 * Created by eren.sinecan on 02/04/2017.
 */

/**
 * Implementation of Insulin treatment method.
 * If a person has Diabetes without being administered Insulin,
 * a 40 day wait will kill them.
 * In other cases, a 40 day wait does not affect a patient's health
 */
public class Wait40Days implements TreatmentContract {

    @Override
    public HealthCondition treat(Patient patient) {
        HealthCondition healthCondition = patient.getCurrentHealthCondition();
        if(!patient.getTreatments().contains(Treatment.INSULIN)
                && (patient.getCurrentHealthCondition() == HealthCondition.DIABETES
                    || patient.getCurrentHealthCondition() == HealthCondition.DEAD)){
            healthCondition = HealthCondition.DEAD;
        }
        patient.addTreatment(Treatment.WAIT_40_DAYS);
        return healthCondition;
    }
}
