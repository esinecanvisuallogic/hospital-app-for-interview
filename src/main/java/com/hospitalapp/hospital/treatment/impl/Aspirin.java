package com.hospitalapp.hospital.treatment.impl;

/**
 * Created by eren.sinecan on 02/04/2017.
 */

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.treatment.TreatmentContract;
import com.hospitalapp.hospital.domain.Treatment;
import lombok.NoArgsConstructor;

/**
 * Implementation for Aspirin treatment.
 * If used with paracetamol, it kills the patient.
 * Otherwise, it will cure fever if the patient has it.
 * No other side effects.
 */
@NoArgsConstructor
public class Aspirin implements TreatmentContract {

    @Override
    public HealthCondition treat(Patient patient) {
        HealthCondition healthCondition = patient.getCurrentHealthCondition();
        if(patient.getTreatments().contains(Treatment.PARACETAMOL)){
            healthCondition = HealthCondition.DEAD;
        }else{
            if(patient.getCurrentHealthCondition() == HealthCondition.FEVER)
            {
                healthCondition = HealthCondition.HEALTHY;
            }
        }
        patient.addTreatment(Treatment.ASPIRIN);
        return healthCondition;
    }
}
