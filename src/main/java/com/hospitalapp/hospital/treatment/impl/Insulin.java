package com.hospitalapp.hospital.treatment.impl;

import com.hospitalapp.hospital.domain.HealthCondition;
import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.treatment.TreatmentContract;

import static com.hospitalapp.hospital.domain.HealthCondition.FEVER;
import static com.hospitalapp.hospital.domain.HealthCondition.HEALTHY;
import static com.hospitalapp.hospital.domain.Treatment.ANTIBIOTIC;
import static com.hospitalapp.hospital.domain.Treatment.INSULIN;

/**
 * Created by eren.sinecan on 02/04/2017.
 */

/**
 * Implementation of Insulin treatment method.
 * If a person is initially healthy, insulin will give them
 * fever if taken with antibiotic.
 * If a person has diabetes, insulin will allow them to survive
 * a 40 day wait without actually curing them permanently and will
 * not cause fever if taken with insulin in this case as deduced by
 * test specs.
 * Otherwise insulin will not change the health status
 */
public class Insulin implements TreatmentContract {

    @Override
    public HealthCondition treat(Patient patient) {
        HealthCondition healthCondition = patient.getCurrentHealthCondition();
        if(patient.getTreatments().contains(ANTIBIOTIC)
                && patient.getInitialHealthCondition() == HEALTHY){
            healthCondition = FEVER;
        }
        patient.addTreatment(INSULIN);
        return healthCondition;
    }
}
