package com.hospitalapp.hospital;

import com.hospitalapp.hospital.builders.PatientListBuilder;
import com.hospitalapp.hospital.builders.ReportBuilder;
import com.hospitalapp.hospital.domain.Patient;
import com.hospitalapp.hospital.domain.Treatment;
import java.util.List;

import static com.hospitalapp.hospital.domain.Treatment.*;

public class Quarantine {

    private List<Patient> patients;

    public Quarantine(String subjects) {
        patients = PatientListBuilder.build(subjects);
    }

    public void aspirin() {
        applyTreatmentToEveryone(ASPIRIN);
    }

    public void antibiotic() {
        applyTreatmentToEveryone(ANTIBIOTIC);
    }

    public void insulin() {
        applyTreatmentToEveryone(INSULIN);
    }

    public void paracetamol() {
        applyTreatmentToEveryone(PARACETAMOL);
    }

    public void wait40Days() {
        applyTreatmentToEveryone(WAIT_40_DAYS);
    }

    public String report() {
        return ReportBuilder.build(patients);
    }

    /**
     * This method takes a treatment and applies it to every patient
     * in the quarantine. Uses parallelStream as patients' treatment
     * outcomes do not affect each other.
     * @param treatment
     */
    private void applyTreatmentToEveryone(Treatment treatment){
        patients.parallelStream().forEach(patient -> {
            patient.setCurrentHealthCondition(treatment.treat(patient));
        });
    }
}
